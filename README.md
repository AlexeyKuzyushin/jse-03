# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO
**NAME**: ALEXEY KUZYUSHIN

**E-MAIL**: alexeykuzyushin@yandex.ru

# SOFTWARE

- JDK 1.8
- Ubuntu 20.10

# PROGRAM RUN

``` bash
java -jar ./task-manager.jar
```
